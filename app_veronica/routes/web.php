<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[UserController::class, 'login'])->name('login');
Route::post('/login',[UserController::class, 'session_login'])->name('app.user.login');


Route::get('/index',[UserController::class, 'index'])->name('app.index')->middleware('session.validate');
Route::get('/json/users',[UserController::class, 'list_users'])->name('app.json.users')->middleware('session.validate');
Route::post('/user/create',[UserController::class, 'create_user'])->name('app.user.create')->middleware('session.validate');
Route::post('/user/update',[UserController::class, 'update_user'])->name('app.user.update')->middleware('session.validate');
Route::post('/user/session/close',[UserController::class, 'session_close'])->name('app.session.close')->middleware('session.validate');
Route::delete('/user/delete/{id}',[UserController::class, 'delete_user'])->name('app.user.delete')->middleware('session.validate');
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
