<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Actions;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        
            $data = [
                ['name'=>'INCIO DE SESSION',
                'description'=>'Inicio session de Usuario'],

                ['name'=>'CERRAR SESSION',
                'description'=>'Cierre de session'],

                ['name'=>'CREACION DE USUARIO',
                'description'=>'Creacion de usuario'],

                ['name'=>'ACTUALIZACION DE USUARIO',
                'description'=>'Actualizacion de usuario'],

                ['name'=>'ELIMINACION DE USUARIO',
                'description'=>'Eliminacion de usuario'],
            ];
            
            
        foreach ($data as $data_create) {
            Actions::factory()->create($data_create);
        }
            
    }
}
