<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Audits;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{

    public function login()
    {   
        return view('login.login');
    }


    public function session_login(Request $request)
    {
        $query_login = User::where('name',trim($request->usuario))->first();

        //dd($query_login);
        if (!empty($query_login)) {
            if(Crypt::decrypt($query_login->password) == $request->password){

                session()->put('usuario',$request->usuario);

                $url = route('app.index');
                $this->audit(1,'Inicio de session '.$request->usuario);
                
                return response()->json(['status'=>200,'data'=>$url], 200);
                
            }else{
                return response()->json(['status'=>401,'data'=>'session Incorrect'], 401);
            }
        }else{
            return response()->json(['status'=>401,'data'=>'session Incorrect'], 401);
        }
    }




    public function create_user(Request $request)
    {
        try {
            
            $user_insert =new User();
            $user_insert->name = $request->name;
            $user_insert->email =  $request->email;
            $user_insert->password = Crypt::encrypt($request->password); ;
            $user_insert->save();

            $this->audit(3,'Creacion del usuario '.$request->name);

            return response()->json(['status'=>200,'data'=>'User Insert'], 200);
        } catch (Exception $e) {
            return response()->json(['status'=>401,'data'=>'User Not Insert'], 401);
        }
    }


    public function update_user(Request $request)
    {
        try {
            
            $user_update =User::find($request->id);
            $user_update->name = $request->name;
            $user_update->email =  $request->email;
            $user_update->password = Crypt::encrypt($request->password); ;
            $user_update->save();

            $this->audit(4,'Actualizacion del usuario '.$request->name);

            return response()->json(['status'=>200,'data'=>'User Update'], 200);
        } catch (Exception $e) {
            return response()->json(['status'=>401,'data'=>'User Not Update'], 401);
        }
    }

    public function list_users()
    {
        $query_users = User::all();
        $auth_user = session()->get('usuario');

        if(is_object($query_users) || !empty($query_users)){
            
            $response_data = array(); 
            foreach ($query_users as $user) {
                $user_array = array(
                'id'=>$user->id,
                'name'=>$user->name,
                'email'=>$user->email,
                'password'=>Crypt::decrypt($user->password));
                
                $response_data[] =$user_array; 
            }

            if(count($response_data) >= 1){
                return response()->json(['status'=>200,'data'=>$response_data,'user'=>$auth_user], 200);
            }else{
                return response()->json(['status'=>401,'data'=>'Not Data'], 401);
            }   
        }else{
            return response()->json(['satus'=>401,'data'=>'Not Data'], 401);
        }
    }

    public function delete_user($id)
    {
        try {
            
            $user_delete =User::where('id',$id)->delete();
            
            $this->audit(5,'Eliminacion del usuario '.$id);
            
            return response()->json(['status'=>200,'data'=>'User Delete'], 200);
        } catch (Exception $e) {
            return response()->json(['status'=>401,'data'=>'User Not Delete'], 401);
        }        
    }

    public function session_close()
    {
        try {
            $this->audit(2,'Session cerrada del usario '.session()->get('usuario'));
            
            session()->flush();
            $url = route('login');

            return response()->json(['status'=>200,'data'=>$url], 200);
        } catch (Exception $e) {
            return response()->json(['status'=>401,'data'=>'User Not Close Session'], 401);
        }
    }

    public function audit($id_action,$description)
    {
       
        $userq= session()->get('usuario');
        $data_id_user = User::select('id')->where('name',$userq)->first();
        $user_audit = $data_id_user->id;
        
        $insert_audit = new Audits();
        $insert_audit->user_id = $user_audit;
        $insert_audit->action_id = $id_action;
        $insert_audit->description = $description;
        $insert_audit->save();
    }


    public function index()
    {
        return view('user.index');
    }
}
