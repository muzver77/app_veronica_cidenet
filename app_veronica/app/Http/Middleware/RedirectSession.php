<?php

namespace App\Http\Middleware;

use Closure;

class RedirectSession 
{
    public function handle($request, Closure $next)
    {   
        if (empty(session()->get('usuario'))) {
            return redirect()->route('login');
        }else{
            return $next($request);
        }

        abort(403);
    }
}