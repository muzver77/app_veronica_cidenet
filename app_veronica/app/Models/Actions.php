<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actions extends Model
{
    use HasFactory,SoftDeletes;

    protected $table='actions';
    protected $connection='mysql';

    protected $fillable = [
        'id',
        'name',
        'description',
    ];
}
