<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Audits extends Model
{
    use HasFactory,SoftDeletes;

    protected $table='audits';
    protected $connection='mysql';

    protected $fillable = [
        'id',	
        'user_id',	
        'action_id',	
        'description',
    ];

}
